---
title: Disroot is
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
text_align: center
body_classes: modular
---

<br>
## Disroot is a platform providing online services based on principles of freedom, privacy, federation and decentralization.
**No tracking, no ads, no profiling, no data mining!**
