---
title: 'ടിസറൂഠ് അപ്ലിക്കേഷൻ'
bgcolor: '#1F5C60'
fontcolor: '#fff'
text_align: left
wider_column: right
---

<br>

## [![എഫ്-ഡ്രോയിഡിൽ](F-Droid.svg.png?resize=80,80&class=imgcenter)](https://f-droid.org/en/packages/org.disroot.disrootapp/) [F-droid](https://f-droid.org/en/packages/org.disroot.disrootapp/?class=lighter) നിന്ന് അപ്ലിക്കേഷൻ ഇൻസ്റ്റാൾ ചെയ്യുക 

---

## ടിസറൂഠ് അപ്ലിക്കേഷൻ
ഈ അപ്ലിക്കേഷൻ കമ്മ്യൂണിറ്റിക്കായി കമ്മ്യൂണിറ്റി നിർമ്മിച്ച ഡിസ്‌റൂട്ട് പ്ലാറ്റ്‌ഫോമിലേക്കുള്ള നിങ്ങളുടെ സ്വിസ് സൈനിക കത്തി പോലെയാണ്. നിങ്ങൾക്ക് ഒരു ഡിസ്‌റൂട്ട് അക്കൗണ്ട് ഇല്ലെങ്കിൽ, ഈതർപാഡ്, അപ്‌ലോഡ് മുതലായ അക്കൗണ്ട് ആവശ്യമില്ലാത്ത എല്ലാ ഡിസ്‌റൂട്ട് സേവനങ്ങളും ആക്‌സസ് ചെയ്യാൻ നിങ്ങൾക്ക് ഈ അപ്ലിക്കേഷൻ ഉപയോഗിക്കാം.
