---
title: ഓൺലൈൻ-സംഭാവന
bgcolor: '#fff'
wider_column: left
fontcolor: '#1e5a5e'
---

![](premium.png?class=reward) ഡിസ്‌റൂട്ട് അതിന്റെ കമ്മ്യൂണിറ്റിയിൽ നിന്നും സേവന ഉപയോക്താക്കളിൽ നിന്നുമുള്ള സംഭാവനകളെയും പിന്തുണയെയും ആശ്രയിക്കുന്നു. പ്രോജക്റ്റ് തുടരാനും പുതിയ ഡിസ്റൂട്ടുകാറക്ക് ഇടം സൃഷ്ടിക്കാൻ സഹായിക്കാനും നിങ്ങൾ ആഗ്രഹിക്കുന്നുവെങ്കിൽ, സാമ്പത്തിക സംഭാവന നൽകാൻ ലഭ്യമായ ഏതെങ്കിലും രീതികൾ ഉപയോഗിക്കുക.[Aliases and domain linking](/services/email#alias) പതിവായി പിന്തുണക്കുന്നവറ്ക്കു ലഭ്യമാണ്.
[അധിക ഇമെയിൽ സംഭരണ സ്ഥലവും](/services/email#storage) കൂടാതെ / അല്ലെങ്കിൽ [അധിക ക്ലൗഡ് സംഭരണ സ്ഥലവും](/services/nextcloud#storage) വാങ്ങുന്നതിലൂടെ നിങ്ങൾക്ക് സഹകരിക്കാനാകും.

---

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Donate using Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Become a Patron" src="donate/_donate/p_button.png" /></a>

<a href="https://flattr.com/profile/disroot" target=_blank><img alt="Flatter this!" src="donate/_donate/f_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=en&hosted_button_id=AW6EU7E9NN3VQ" target=_blank><img alt="Paypal"  src="donate/_donate/pp_button.png" /></a>

<a href="https://disroot.org/cryptocurrency"><img alt="Cryptocurrency" src="donate/_donate/c_button.png" /></a>

</div>

<span style="color:#4e1e2d; font-size:1.1em; font-weight:bold;">ബാങ്ക് കൈമാറ്റം</span>
<span style="color:#4e1e2d; font-size:1.1em;">
Stichting Disroot.org
IBAN NL19 TRIO 0338 7622 05
BIC TRIONL2U
</span>
