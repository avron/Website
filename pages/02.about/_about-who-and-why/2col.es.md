---
title: Email
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
body_classes: modular
wider_column: right
---

<a class="button button1" href="https://disroot.org/es/mission-statement">Lee nuestra Declaración de Objetivos</a>

---

**Disroot** es un proyecto radicado en Amsterdam, mantenido por voluntarixs y que depende del apoyo de su comunidad.


Originalmente, creamos **Disroot** por necesidades personales. Buscábamos software que pudiéramos usar para comunicarnos, compartir y organizarnos dentro de nuestros círculos. La mayoría de las soluciones disponibles carecían de los elementos claves que nosotros consideramos importantes: nuestras herramientas debían ser **abiertas**, **descentralizadas**, **federadas** y **respetuosas** de la **libertad** y la **privacidad**.

Mientras buscábamos esas herramientas, encontramos algunos proyectos realmente interesantes, proyectos que creemos que deberían estar disponibles para cualquiera que valore principios similares. Por lo tanto, decidimos reunir algunas aplicaciones y compartirlas con otras personas. Así es cómo **Disroot** empezó.

Con el funcionamiento de **Disroot** esperamos cambiar la manera en que la gente habitualmente interactúa en la web. Queremos alentar a las personas a **liberarse** de los ["jardines vallados"](https://es.wikipedia.org/wiki/Jard%C3%ADn_vallado_(inform%C3%A1tica)) del software popular y cambiar a alternativas abiertas y éticas, ya sea en nuestra plataforma o en otra (o incluso en la tuya propia).

Juntos podemos construir una red que sea verdaderamente independiente, enfocada en el beneficio de la gente y no en su explotación.
