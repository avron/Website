---
title: 'Nutze Deinen bevorzugten Email-Client'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

<br>
# Nutze das Email-Programm Deiner Wahl
Es gibt Unmengen Email-Clients für den Desktop oder Dein Mobilgerät. Such Dir den Client aus, der Dir am besten gefällt.<br>

Schau Dir unser [Tutorial](https://howto.disroot.org/de/tutorials/email/clients) an, um nähere Informationen zum Setup Deines bevorzugten Clients zu erhalten.
<br>
*Oben auf dieser Seite findest Du die Einstellungen, um Deine* **Disroot**-*Email auf Deinem Gerät einzurichten. Diese Einstellungen sind die Standardinformationen, die Deinem Email-Client sagen, wie er den* **Disroot**-*Emailserver erreicht.*
