---
title: 'Email Clients'
bgcolor: '#FFFFFF'
fontcolor: '#327E82'
text_align: center
---

<br>
# Use your favorite Email client
There are a lot of desktop/mobile email clients to choose from. Pick the one you like the best.<br>

Check our [Howto page](https://howto.disroot.org/en/tutorials/email/clients) for details on how to setup your favourite clients.
<br>
*Check the settings on top of this page to set up your* **Disroot** *email on your device. These settings are standard information that tells your email client how to reach* **Disroot**'*s email server.*
