---
title: 'Messaggi di posta criptati'
bgcolor: '#8EB726'
fontcolor: '#1F5C60'
text_align: left
wider_column: right
---

## Che cos'e la crittografia

---

La crittografia avviene quando si modificano i dati con uno speciale processo di codifica in modo che i dati diventino irriconoscibili (sono crittografati). È quindi possibile applicare un processo di decodifica speciale e si otterranno i dati originali. Mantenendo il processo di decodifica un segreto, nessun altro può recuperare i dati originali dai dati crittografati.
https://www.youtube.com/watch?v=E5FEqGYLL0o
