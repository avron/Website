---
title: 'Lesezeichen'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Wähle Deinen bevorzugten Client'
clients:
    -
        title: Bookmarks
        logo: de/bookmarks_logo.png
        link: https://f-droid.org/de/packages/org.schabi.nxbookmarks/
        text:
        platforms: [fa-android]
---

## Lesezeichen

Eine Lesezeichen-Verwaltung für **Nextcloud**. Speichere Deine Lieblingsseiten und synchronisiere sie auf mehreren Geräten.

---

![](de/Bookmarks.png?lightbox=1024)
