---
title: 'Deck'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
cloudclients: true
clients_title: 'Choose your favorite client'
clients:
    -
        title: Deck
        logo: en/deck_logo.png
        link: https://f-droid.org/en/packages/it.niedermann.nextcloud.deck/
        text:
        platforms: [fa-android]
---

![](en/nextcloud-deck.png?lightbox=1024)

---

## Deck

Deck helps you organize your personal or team work within Nextcloud. You can add tasks, classify them, share them and comment on them.
