---
title: 'Nextcloud Keep or Sweep'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](de/nextcloud-sweep.png?lightbox=1024)

---

## Keep or Sweep

Diese Funktion hilft Dir, Durcheinander zu vermeiden, indem sie Dich daran erinnert Dateien zu entfernen, die Du vielleicht vergessen hast.
