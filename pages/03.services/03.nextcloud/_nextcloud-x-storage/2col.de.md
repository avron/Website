---
title: 'Cloudspeicher'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
section_id: storage
---

## Cloud-Speicherplatz

Mit Deinem **Disroot**-Account erhältst Du 2 GB FREIEN Speicher.
Du kannst diesen Speicherplatz von 2 GB auf 14, 29 oder 54 GB erhöhen für nur 0,15 €/Monat. Hier kannst Du dann sogar wählen, ob Du einen Teil oder den ganzen zusätzlichen Speicher für Dein Mailpostfach nutzen willst.

Um Extraspeicher zu beantragen, fülle bitte dieses [Formular](https://disroot.org/de/forms/extra-storage-space) aus.<br>Wenn Du einen Teil des Extraspeichers oder alles für Dein Mailpostfach nutzen willst, vermerke das bitte im Kommentarfeld des Formulars.


---

<br><br>

<a class="button button1" href="https://disroot.org/de/forms/extra-storage-space">Extraspeicher beantragen</a>
