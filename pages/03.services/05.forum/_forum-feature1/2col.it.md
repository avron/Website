---
title: 'Caratteristiche - 1'
wider_column: right
---

![](en/discourse_feature1.png?lightbox=1024)

---
# Mailing list e forum di discussione
Oltre che come forum, Discourse può pure essere utilizzato come una mailing-list. In questo modo è possibile gestire i flussi comunicativi nel miglior modo possibile rispettando le proprie abitudini e preferenze. È possibile aprire nuovi topic e rispondere mandando un messaggio di posta elettronica oppure utilizzando la classica interfaccia web dei forum.

# Interfaccia mobile friendly
Discourse è fruibile in modo molto semplice da qualsiasi device. Non sono necessarie app esterne.

# Gruppi pubblici vs gruppi privati
È possibile aprire forum pubblici o privati automoderati. L'amministratore del gruppo ha inoltre la possibilità di gestire le partecipazioni ai vari forum.
