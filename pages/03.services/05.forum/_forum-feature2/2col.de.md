---
title: 'Feature 2'
wider_column: left
---

## Eine gute Benutzeroberfläche
Die Benutzeroberfläche ist äußerst praktisch:

- Dynamische Benachrichtigungen werden Dir eingeblendet, wenn jemand auf einen Beitrag von Dir antwortet, Dich zitiert oder Deinen Namen erwähnt. Wenn Du nicht online bist erhältst Du eine Benachrichtigung per Email.
- Keine Diskussionen mehr, die auf mehrere Seiten verteilt sind. Der Diskussionsbaum wird fortlaufend angezeigt, während Du nach unten scrollst.
- Antworte bereits, während Du noch liest. Mit Hilfe eines geteilten Bilsdschirms kannst Du weiter Antworten und Beiträge anderer Nutzer lesen und gleichzeitig Zitate, Verknüpfungen oder Verweise einfügen.
- Echtzeit-Aktualisierungen. Wenn eine Diskussion sich ändert während Du sie liest oder eine Antwort schreibst, wird sie sich automatisch aktualisieren.
- Von jeder Seite aus suchen - fang einfach an zu tippen und Du erhältst noch währenddessen Ergebnisse.

---

![](de/discourse_feature2.png?lightbox=1024)
