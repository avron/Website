---
title: Discourse
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Für Disroot registrieren</a>
<a class="button button1" href="https://forum.disroot.org/">Nur im Forum anmelden oder registrieren</a>

---
![](discourse_logo.png)

## Moderner Ansatz eines Diskussionsforums

Disroots Forum wird bereitgestellt von **Discourse**. Discourse ist ein vollständig quelloffener, moderner Ansatz eines Diskussionsforums. Es bietet alles, was Deine Gemeinschaft, Deine Gruppe oder sogar Deine Familie benötigt, um ihre eigene Kommunikationsplattform zu erstellen, öffentlich oder privat. Es ermöglicht dem Einzelnen, Diskussionen zu Themen, die ihn interessieren, zu erstellen und mit anderen darüber zu diskutieren oder auch einfach in bestehende Gemeinschaften einzusteigen.

Disroot-Forum: [https://forum.disroot.org](https://forum.disroot.org)

Projekt-Website: [http://discourse.org](http://discourse.org)

Quellcode: [https://github.com/discourse/discourse](https://github.com/discourse/discourse)
