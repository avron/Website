---
title: 'File storage'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](drive.png)

## Encrypted File storage
Upload and share any file. All stored files are end-to-end encrypted!



---
![](code.png)

## Collaborative Code editor
Edit your code together with your team members while having it end-to-end encrypted with zero knowledge server side.
