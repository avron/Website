---
title: 'Polls'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](polls.png)

## End to End Encrypted Polls
**Polls** offer you to create and share totally end-to-end encrypted polls.


---
![](presentation.png)

## Encrypted Presentations on the fly
Create end-to-end encrypted presentations with easy editor and together with your friends or team members. Your finished presentation can be "played" directly from cryptpad.
