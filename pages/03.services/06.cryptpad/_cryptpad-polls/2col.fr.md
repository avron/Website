---
title: 'Sondages'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---
![](polls.png)

## Sondages chiffrés de bout en bout
Les **Sondages** vous permettent de créer et de partager des sondages totalement chiffrés de bout en bout.


---
![](presentation.png)

## Présentations chiffrées
Créez des présentations chiffrées de bout en bout avec un éditeur simple et en collaboration avec vos amis ou les membres de votre équipe. Votre présentation terminée peut être "jouée" directement depuis le cryptpad.
