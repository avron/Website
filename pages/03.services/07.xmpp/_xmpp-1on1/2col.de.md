---
title: 'Xmpp Direktnachrichten'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](monal.jpg)

---

## Private Chats

Rede ungehindert mit jedem anderen Disrooter oder mit jedem da draußen, der irgendeinen XMPP-komatiblen Server im Internet nutzt. Du entscheidest, ob ein Verlauf der Unterhaltung auf dem Server gespeichert werden soll und ob Du eine Verschlüsselung nutzen möchtest. Benutzernamen bei XMPP gleichen vom Aufbau her dem von Email-Adressen enstsprechend des XMPP-Servers (Nutzername@Beispiel.de).
