---
title: 'Messagerie Directe Xmpp'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](monal.jpg)

---

## Discussions privées

Parlez librement avec d'autres Disrooters et même avec toute personne sur Internet utilisant un serveur Xmpp compatible. Vous décidez vous-même si vous voulez garder un historique de vos chats sur le serveur ou si vous voulez activer l'encryption. Les noms d'utilisateurs sur XMPP ressemblent à des adresses emails en relation avec le serveur XMPP (nomdutilisateur@serveurexemplexmpp.com).
