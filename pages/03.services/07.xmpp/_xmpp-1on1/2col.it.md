---
title: 'Xmpp Direct Messaging'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](monal.jpg)

---

## Chat uno a uno

Parla liberamente con qualsiasi altro utente di disroot o chiunque abbia un account jabber.

Decidi se memorizzare o meno la cronologia della chat sul server o abilitare la crittografia.

Il nome utente su XMPP è uguale all'indirizzo email corrispondente al server XMPP (nomeutente@esempio.com).
