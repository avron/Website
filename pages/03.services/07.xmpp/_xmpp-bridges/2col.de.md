---
title: 'Xmpp Bridges'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](conversations.jpg)

---

## Gateways, Bridges und Transports

XMPP erlaubt verschiedene Wege, um sich mit verschiedenen Chat-Protokollen zu verbinden. Du kannst Dich über unser Biboumi IRC Gateway mit jedem IRC-Raum verbinden. Für die Zukunft planen wir eine Matrix-Bridge (derzeit noch über bau-haus.net, unsere Internet-Freunde), eine Telegram-Bridge und eine Vielzahl von anderen Bridges und Transports, die wir gerade vorbereiten.
