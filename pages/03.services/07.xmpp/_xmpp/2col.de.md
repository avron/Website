---
title: XMPP
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<br><br><br><br>
<a class="button button2" href="https://user.disroot.org/pwm/public/newuser">Bei Disroot registrieren</a>
<a class="button button1" href="https://webchat.disroot.org/">Anmelden per Webchat</a>

---
![](xmpp_logo.png?resize=128,128)

# XMPP
### Dezentralisiertes und Föderatives Instant Messaging

Kommuniziere über ein standardisiertes, offenes und föderatives Chatprotokoll mit der Möglichkeit, Deine Kommunikation mit dem OMEMO-Protokoll zu verschlüsseln (basierend auf den Verschlüsselungsmethoden, die von Anbietern wie Signal und Matrix genutzt wird). Mit XMPP bist Du nicht an einen Serviceanbieter gebunden (z.B. Disroot), sondern kannst ungehindert mit Kontakten auf anderen [Jabber-Servern](https://compliance.conversations.im/) kommunizieren, genau so wie Du zwischen verschiedenen Email-Servern kommunizieren würdest.


Projekt-Website: [https://xmpp.org](https://xmpp.org)
Server-Implementierung: [https://prosody.im](https://prosody.im)

<a href='https://compliance.conversations.im/server/disroot.org'><img src='https://compliance.conversations.im/badge/disroot.org'></a>
