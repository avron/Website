---
title: 'Caratteristiche di Etherpad'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Evidenzia con i colori

Tutte le modifiche fatte sul testo vengono evidenziate con un colore al quale è associato un utente.

## Formattazione del testo

È possibile utilizzare il grassetto, l'italico, struttura a paragrafi, punti elenco e altro ancora.

## Chat

È attiva una chat per tutti gli utenti che stanno lavorando al testo.

## Cronologia

È possibile vedere la cronologia dei cambiamenti effettuati sul testo.

---

![](en/etherpad-example.png?lightbox=1024)
