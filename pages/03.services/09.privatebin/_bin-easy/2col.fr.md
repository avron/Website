---
title: 'Simple'
wider_column: right
---

![](en/privatebin01.gif)

---

# Vraiment facile à utiliser

<ul class=disc>
<li>Coller du texte, cliquer sur "Envoyer", partager l'URL.</li>
<li>Définissez une limite d'expiration : 5 minutes, 10 minutes, 1 heure, 1 jour, 1 semaine, 1 mois, 1 an ou jamais.</li>
<li> Option "Brûler après lecture" : La pâte est détruite lors de la lecture. </li>
<li> URL de suppression unique générée pour chaque coller.</li>
<li>Coloration de la syntaxe pour 54 langues (en utilisant highlight.js), support du mixage (html/css/javascript).</li>
<li>Conversion automatique des URL en liens cliquables (http, https, ftp et magnet).</li>
<li>Bouton unique pour cloner une pâte existante.</li>
<li>Discussions : Vous pouvez activer la discussion sur chaque paste.</li>
</ul>
