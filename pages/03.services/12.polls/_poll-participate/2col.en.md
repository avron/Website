---
title: 'Participate in poll'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Participate in the poll
Choose your preferences without a need to create account. **Framadate** allows for extensive discussion within the poll letting you perticipate in the decission beyond just ticking your prefered choice.

---

![](en/poll_participate.gif)
