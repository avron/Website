---
title: 'Créer un sondage'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/poll_results.gif)

---

# Voir les résultats
Revenez sur le sondage pour voir les résultats. La vue graphique facilite la prise de décision finale en vous permettant d'avoir une meilleure vue d'ensemble de tous les choix effectués par les participants. Le résultat du sondage peut être téléchargé sous forme de fichier csv sur votre ordinateur.
