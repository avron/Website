---
title: Sondages
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://poll.disroot.org/">Créer un sondage</a>

---

![](framadate_logo.png)

Les sondages Disroot sont gérés par **Framadate**. Framadate est un service en ligne pour planifier un rendez-vous ou prendre une décision rapidement et facilement. Créez votre sondage, partagez-le avec vos amis ou collègues pour qu'ils puissent participer au processus de décision et obtenir les résultats !

Vous n'avez besoin d'aucun compte sur Disroot pour utiliser ce service.

Sondage Disroot: [https://poll.disroot.org](https://poll.disroot.org)

Code source: [https://git.framasoft.org/framasoft/framadate](https://git.framasoft.org/framasoft/framadate)
