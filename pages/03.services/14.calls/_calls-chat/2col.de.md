---
title: 'Chat'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

# Chat
Nutze einen Textchat parallel zu Deiner Video-/Audiokonferenz.

---

![](chat.png)
