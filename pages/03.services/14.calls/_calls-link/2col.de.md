---
title: 'Angepasster Link'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](link.png)

---

# Angepasster Link
Du kannst einen eigenen Namen für Deinen Konferenzraum wählen, dieser wird dann die Adresse Deiner Konferenz sein.
