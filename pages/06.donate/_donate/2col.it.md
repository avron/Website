---
title: Online-donation
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---


# "Se non paghi per il prodotto, il prodotto sei tu."

---

#### Donazioni online:

<div class="donate">

<a href="https://liberapay.com/Disroot/donate" target=_blank><img alt="Dona con Liberapay" src="donate/_donate/lp_button.png" /></a>

<a href="https://www.patreon.com/bePatron?u=8269813" target=_blank><img alt="Diventa un Patron" src="donate/_donate/p_button.png" /></a>

<a href="https://flattr.com/profile/disroot" target=_blank><img alt="Flatter this!" src="donate/_donate/f_button.png" /></a>

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&lc=it_IT&hosted_button_id=AW6EU7E9NN3VQ" target=_blank><img alt="Paypal"  src="donate/_donate/pp_button.png" /></a>

<a href="/cryptocurrency"><img alt="Cryptocurrency" src="donate/_donate/c_button.png" /></a>

</div>

#### Riferimenti bancari:
<span style="color:#8EB726; font-size:1.8em;"> Stichting Disroot.org <br>
IBAN NL19 TRIO 0338 7622 05<br>
BIC TRIONL2U
</span>

Carte di credito: <br><span style ="color:#8EB726;">
È possibile utilizzare il pulsante blu Paypal per le donazioni di carte di credito, non è necessario un account Paypal. </Span>

#### Donazioni di componenti hardware: <span style="color:#8EB726;"> Vedi sotto </span>
