---
title: 'End of 2016 sprint summary'
date: 12/28/2016
taxonomy:
  category: news
  tag: [disroot, news, nextcloud, dashboard, matrix, spam-bot]
body_classes: 'single single-post'

---


## Hi Disrooters,

**First of we would like to wish you all a happy new year. It’s been a joy running Disroot for the past year. A lot has changed since we started. We had our fun and our stress moments, we learned a lot and we managed to stay afloat. We can only wish the next year to be as rich as this one was. We wish you the same in your personal and professional life.**

This is the last sprint of the year. We’ve focus on Instant chat messaging which took an unexpected turn during the sprint, resulting in setting up a brand new service, we are super excited about. In addition to that, a new version of Nextcloud came around with a lot of improvements and so we took the opportunity to update it and share new features with you. Antilopa has been busy creating Disroot app dashboard and some of the disrooters decided to jump in and help us with writing tutorials.

We can most definitely say that Disroot is getting more lively these days and we think we will see an even bigger boost of user base in the coming year. Considering that the “popularity” of Disroot is growing, we must start thinking more seriously about the financial stability of the project – so our infrastructure can grow as the community grows. In the upcoming sprints we will focus on finding ways to get regular flow of income to cover our current and future costs. We will look into regular group contribution, plea for individual donations, think of possible merchandise and look for subsidies that can co-exist with our values and intentions. Our second objective during the upcoming sprints will be to complete many needed tutorials and howtos.

----------
**Hell, why wait? Let us start with a little pleading right now;**

We all heard that nothing is free. Well, there is no such thing as a free service on the internet. The costs of hardware, electricity and server space, are quite high. So when you use a “free” service, you most probably pay for it one way or another. Companies cover the costs of running their services by charging you actual money or by trading your private data with advertising agencies or governments. So far Disroot is running on few irregular donations and much of our personal investment, but that cannot sustain a growing platform. However, if every Disroot user would decide to donate just 1EURO (or whatever currency you’re comfortable with) a month, all our costs, investments and maybe even some of the time we spend on this project could be covered. All it takes is 12“euros“ a year from you to keep this project running and create space for others to make use of Disroot. This is the equivalent of a drink in a bar, few beers, or a pack of cigarettes. Please keep that in mind and consider a regular contribution. Or an irregular contribution – anything helps.

-----------

Back to our summary; Here are some more details on whats new:

### [DASHBOARD](https://apps.disroot.org)
You can now find quick access links to all Disroot services with new dashboard at [https://apps.disroot.org](https:apps.disroot.org)

### NEXTCLOUD
Nextcloud has been updated to its latest version – Nextcloud 11 includes many new features:

 – Nextcloud 11 comes with an integrated audio and video chat app. So we are dropping the cumbersome spreed.me in favor of the Spreed call, for built-in one-on-one conversations and conferences. Apart from notifying your cloud contacts when they are invited to join a call, Spreed also allows sending public link to people who don’t have an account yet. This app is still very experimental, but there are plenty of features queuing up so expect a lot more cool stuff soon. We would like to gather your feedback about the quality of calls, the interface etc, so don’t hesitate and contact us.

 – The new version got a general performance boost.

 – And an improved user interface:

    - The blue bar at the top has gotten a function – menu items will now appear there instead of in the drop down menu, so all options are visible at all times. You can reorder the menu items by simply dragging and dropping them into desired place.
    - The new activity panel now includes changes to files, calendar, comments and Todo lists
    - Moving files just got easier with the ‘Move’ option added to the file’s menu
    - The Contacts and Calendar apps also have seen a number of improvements, most notably by introducing public sharing links in the Calendar and improved contacts import in the Contacts app.
    - Dashboard – easy access to all other Disroot services via dashboard app.


## DIASPORA *
The new version of Diaspora is supporting notifications without the need to refresh the page. Also check out the new dark theme in your personal settings.

## MAIL SPAM PROGRAM
Disroot is getting more popular and so we’ve noticed some spam activity recently. We are “teaching“ our spam-bot to automatically detect messages that look like spam and mark them for other users. We do that by scanning Junk folders, which are considered spam, on regular basis. We do not do it on all user accounts, because we can’t be sure only spam emails reside in your Junk folder and we simply do not want to touch your mailbox, even if its done by a computer program (bot). However, if you would like to join our effort to fight spam hitting our Disroot server, please send us an email (support_at_disroot.org) and we will include your account in weekly spam check.

## MATRIX.ORG
As you might read in our [blogpost](https://disroot.org/disroot-joins-the-matrix-network/), while working on improving current XMPP server, we’ve decided to take unexpected turn and launch new service called matrix. these last weeks has been quite a success as we were testing all the functionalities of the new software. We are still testing it out, but you can already join in, say hello and see for yourself what Matrix is all about. We hope to officially announce it in coming weeks.

Last, A big thank to **maryjane** for jumping the wagon and helping us with writing tutorials. We got inspired and made a new years resolution to finally improve our collection of howtos and tutorials.

We hope more people get encouraged – anyone is welcome to join this effort – for an incomplete list of long pending tutorials go to [our task board](https://board.disroot.org/project/disroot-disroot/backlog?tags=howto).
