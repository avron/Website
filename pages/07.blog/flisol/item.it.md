---
title: 'FLISoL 2020'
media_order: flisol_2020.jpg
published: true
date: '14-04-2020 00:30'
taxonomy:
    category:
        - news
    tag:
        - flisol
        - news
body_classes: 'single single-post'

---

Le **Comunità latinoamericane di tecnologie libere e aperte** sono liete di invitarvi ad un'intensa giornata di colloqui, workshop e installazioni dal vivo.

Sabato 25 aprile 2020, a partire dalle 09:00 (UTC-5), avrà luogo una nuova edizione di **FLISoL**. Il **Festival Latinoamericano de Instalación de Software Libre** (Festival latinoamericano di installazione di software libero), è il più grande evento di diffusione e promozione di software libero che si tiene dal 2005.

L'appuntamento è rivolto a tutte le persone interessate al software e sulla cultura del free software. Durante l'incontro, le diverse comunità locali di software libero organizzano eventi legati al software libero. Tra questi pure conferenze, seminari su questioni locali, nazionali e latinoamericane relative sempre al software libero, in tutta la sua gamma di espressioni: artistico, accademico, commerciale e sociale.

Quest'anno, a seguito della pandemia di Coronovirus, il Festival sarà interamente online.

Per ulteriori informazioni e per registrarsi gratuitamente, visita:
[**flisolonline.softlibre.com.ar**](https://flisolonline.softlibre.com.ar)
