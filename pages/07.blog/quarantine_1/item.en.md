---
title: 'Quarantine with Disroot'
media_order: header.jpg
published: true
date: '20-03-2020 18:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - lanparty
        - Teeworlds
        - gaming
body_classes: 'single single-post'
---

**Are you bored yet?**

Since lots of you (us included) are locked in their houses trying to avoid contact with other human beings, we thought it would be nice to spend this time socializing in front of computer with bunch of strangers you know so well from chats, social federated networks and whom you most probably never seen in real life.

Let's spend an evening drinking booze, eating junk, and shooting one another on Teeworlds. **The first edition of Quarantine with Disroot starts on Saturday 21.03.2020 20:00 CET** and will go on until last one standing. Server will probably stay on for the whole week or two if there are people willing to play.

## Let's kick some CoVID butt.![](splashtee3.png)



----
**Quick guide:**
1. To get you started, install Teeworlds either using your package manager if you are using a proper operating system like linux/bsd or go to teeworlds [website](https://teeworlds.com/?page=downloads) and download it for your platform.
2. Once you're done with that, start the game.
3. Fisrt select "Setup" to add your players name, choose and customize your Tee is you like.
4. Hit play and find **"Quarantine with Disroot"** and join us.

The game is rather straight forward and easy to learn, and since probably we are all noobs, no worries we will equally suck at it :)
