---
title: 'New webmail!'
media_order: roundcube.png
published: true
date: '20-04-2020 00:30'
taxonomy:
    category:
        - news
    tag:
        - roundcube
        - webmail
        - news
body_classes: 'single single-post'

---
Dear Disrooters:

Starting from June 8th, we are going to replace the current webmail (powered by **Rainloop**) with a brand new toy powered by **Roundcube**.

For the last 4 years we have been using **Rainloop** for our webmail experience. But due to some missing features and a need to improve the platform's integration, we have decided to re-introduce a webmail environment built on **Roundcube**.

**Roundcube** has been widely used for the last 10 years and it provides the modern, fast, simple and reliable solution one could expect. Here are some feature highlights:

* A nice responsive Disroot flavored skin (and dark mode)
* Better filter support
* Email and mailbox archiving
* Custom themes
* The interface is translated in more than 80 languages.
* Drag-&-drop message management: move your messages with your mouse.
* Full support for MIME and HTML messages.
* Attachment previews: see the images directly within your messages.
* Threaded message listing: Display conversations in a threaded mode.
* Folders: Manage your folders (add, remove, or hide).
* Two factor authentication: add a second level of authentication to your account log-in.

And many more...

In the near future we are planning to integrate it with mailbox encryption as well as **Nextcloud** (calendars, contacts and attachments) and other services currently provided by **Disroot**. We think **Roundcube** will enable us to further improve your email experience.

We hope you'll like the new looks and new features. We sure are looking forward to this switch and future improvements.

**!! Warning for 2FA users!!**

For those of you that have 2FA enabled on the current webmail, you will have to re-set it up again once the new webmail is up and running. Sorry for the inconvenience!
