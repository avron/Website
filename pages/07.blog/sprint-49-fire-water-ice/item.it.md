---
title: 'Fuoco, acqua e ghiaccio'
date: '09-12-2019 15:00'
media_order: 49104435191_596d016b7b_k.jpg
taxonomy:
  category: news
  tag: [disroot, nextcloud, issues, roadmap, sprint, burnout]
body_classes: 'single single-post'

---
*[Sprint 49 - Changement de saison]*

Che montagne russe le ultime settimane!

Siamo partiti da un gigantesco **fuoco** che ha attaccato il nostro server nelle ultime due settimane e mezzo. Il tutto è stato causato dall'aggiornamento di PHP (il backend che alimenta il nostro servizio Nextcloud). Sapevamo che  questo aggiornamento sarebbe stata un'operazione problematica perché avevamo già osservato l'anomalia in passato. Pur avendo preso dei provvedimenti per proteggere il nostro server web, la vulnerabilità scoperta di recente e le voci di ransomware in natura che crittografano intere istanze, ci hanno convinto a fare l'aggiornamento di PHP per essere al riparo da problemi di sicurezza.

Questo aggiornamento se da una parte ha messo in sicurezza il nostro server, come previsto, dall'altra parte ha portato il nostro server ad utilizzare moltissime risorse, rallentando il funzionamento dell'intero cloud. Dopo molti test, modifiche, giorni e notti insonni, cercando di trovare la causa principale, l'abbiamo identificata e successivamente abbiamo messo in atto una soluzione temporanea.  Allo stesso tempo abbiamo comunicato al team di sviluppo di Nextcloud la criticità e ora stiamo aspettando una soluzione permanente. 
L'**acqua** ha quindi spento il fuoco e riportato stabilità sulla piattaforma.

Questa situazione, oltre alle molte responsabilità esterne a Disroot, come ad esempio il lavoro, le famiglie e altri progetti nei quali siamo coinvolti, ci hanno fatto capire che mantenere il nostro ritmo attuale di lavoro costante non poteva continuare nell'attuale impostazione.
A meno che Disroot non diventi la nostra principale occupazione (anche se per qualcuno in realtà praticamente lo è già...) avevamo quindi bisogno di un secchio di **ghiaccio** per rallentare un po'.

Abbiamo quindi deciso di estendere il periodo dello sprint da 2 settimane a 4 (con sprint si intende il tempo necessario per raggiungere degli obiettivi che ci si è posti). Naturalmente continueremo a lavorare costantemente su Disroot, ma con un agio di tempo più diluito e scadenze un po' meno pressanti;).
Per quanto riguarda gli obiettivi generali, sappiamo che in futuro dovremo concentrarci maggiormente sulla sostenibilità finanziaria. Oltre a pagare i costi di hosting e allocare denaro per gli aggiornamenti hardware, vorremmo creare un ecosistema in cui ci si possa impegnare a Disroot, FLOSS e Internet federato e decentralizzato come nostra principale occupazione. 
Abbiamo alcune idee in cantiere. Con l'inizio del nuovo anno saremo più esaustivi.

## Avanti il prossimo...

Il prossimo sprint (il primo di 4 settimane;)) lo abbiamo chiamato *"Catching that roadmap train"*. Ci concentreremo sulla finalizzazione dei punti più importanti dell'attuale roadmap (Q4 2019) per iniziare il nuovo anno con una migliore configurazione, una migliore esperienza di onboarding e utente e idee chiare su cosa fare dopo.
