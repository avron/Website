---
title: 'Vamos a la playa'
date: '13-08-2019 17:00'
media_order: vamos.jpg
taxonomy:
  category: news
  tag: [disroot, news, CoreTeam]
body_classes: 'single single-post'

---

¿Soy yo o tenemos el verano más caliente de la historia? No publicamos nada sobre nuestro último sprint, y esta vez estamos atrasados de nuevo. Por supuesto, podemos echarle la culpa al clima (¡gracias, cambio climático!), pero, hablando seriamente, no hemos pasado ese tiempo en la playa como el título sugiere, aunque probablemente deberíamos haberlo hecho. La razón de este atraso fue que, cargados de nuevas energías, con los nuevos miembros de la pandilla, simplemente, sobrestimamos nuestra capacidad y nos cargamos con más cosas de las que podíamos manejar. Ello resultó en muchas tareas comenzadas pero no tantas finalizadas a tiempo. Aprendida la lección, esta vez decidimos a partir del actual sprint, llamado **"No más de dos Historias de Usuarios por persona"**, cambiar eso e intentar crear un ambiente en el que no nos carguemos tanto. Con suerte, esto nos impedirá generar una situación de constante estrés tratando de cumplir con lo que prometemos pero no llegando a tiempo, lo que nos lleva a quedar quemados, deprimidos, frustrados y otras oscuras y desagradables cosas con las que los humanos tenemos que lidiar. La nueva regla previene eso y crea un ambiente más tranquilo y feliz. Esperamos que salga bien, algo sobre lo que probablemente lean en una semana, cuando terminemos otro sprint.
<br><br>
Mientras tanto, disfruten del clima, y vayan a la playa antes que las catastróficas consecuencias de la existencia humana nos maten a todos.
