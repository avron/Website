---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "KOMMUNIKATION"
section_number: "100"
faq:
    -
        question: "Ich kann nicht auf Matrix zugreifen. Warum?"
        answer: "<p>Wir haben im September 2018 entschieden <b>Matrix</b> nicht mehr zu betreiben. Die Gründe kannst Du <a href='https://disroot.org/en/blog/matrix-closure' target='_blank'>hier</a>.</p>
        <p>Mehr Informationen über die Datenschutzproblematik bei Matrix findest Du <a href='https://github.com/libremonde-org/paper-research-privacy-matrix.org' target='_blank'>hier</a></p>"
    -
        question: "Wie kann ich Disroot's IRC in XMPP-Räumen nutzen?"
        answer: "<p><b>Disroot</b> bietet ein sogenanntes IRC-Gateway. Das heißt, Du kannst jeden IRC-Raum auf jedem IRC-Server betreten. Das grundsätzliche Adress-Schema sieht folgendermaßen aus:</p>
        <ol>
        <li>Um einen IRC-Raum zu betreten: <b>#Raum%irc.domain.tld@irc.disroot.org</b></li>
        <li>Um einen IRC-Kontakt hinzuzufügen: <b>Kontaktname%irc.domain.tld@irc.disroot.org</b></li>
        </ol>
        <p>Dabei ist <b>#Raum</b> der IRC-Raum, den Du betreten willst, und <b>irc.domain.tld</b> ist die IRC-Serveradresse, auf der der Raum gehostet wird. Achte darauf, dass Deine Adresse <b>@irc.disroot.org</b> enthält, da dies die Adresse des IRC-Gateways ist.</p>"
    -
        question: "Ich kann mich mit Disroot-Account nicht bei Diaspora* anmelden."
        answer: "<p>Da <b>Diaspora*</b> leider <b>LDAP</b> (das Software-Protokoll, das wir für den gemeinsamen Zugriff auf Dienste mit einem einzigen Account nutzen) nicht unterstützt, kannst Du Dich nicht mit Deinen <b>Disroot</b>-Zugangsdaten anmelden. Du musst Dir einen eigenständigen <a href='https://pod.disroot.org/' target='_blank'>here</a>. However, registrations are currently closed.</p>"

---
