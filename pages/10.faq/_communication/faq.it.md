---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "COMUNICAZIONE"
section_number: "100"
faq:
    -
        question: "Non riesco a connettermi a Matrix. Perché?"
        answer: "<p>Da settembre 2018 abbiamo deciso di interrompere <b>Matrix</b>. Puoi leggere le motivazioni <a href='https://disroot.org/en/blog/matrix-closure' target='_blank'>qui</a>.</p>
        <p>Puoi trovare ulteriori informazioni sull'impatto sulla privacy di Matrix <a href='https://github.com/libremonde-org/paper-research-privacy-matrix.org' target='_blank'>qui</a></p>"
    -
        question: "Come posso usare l'IRC di Disroot nelle stanze XMPP?"
        answer: "<p><b>Disroot</b> offre un servizio di IRC gateway. Questo significa che dal protocollo XMPP puoi accedere a qualsiasi stanza su server IRC. Per poter accedere ti basta utilizzare il seguente schema:</p>
        <ol>
        <li>Per accedere ad una stanza IRC: <b>#room%irc.domain.tld@irc.disroot.org</b></li>
        <li>Per aggiungere un contatto IRC: <b>contactname%irc.domain.tld@irc.disroot.org</b></li>
        </ol>
        <p>Dove <b>#room</b> è la stanza irc alla quale si vuole accedere e <b>irc.domain.tld</b> è l'indirizzo del server sul quale la stanza è ospitata. Ricordati di non modificare <b>@irc.disroot.org</b> in quanto è  l'indirizzo dell'IRC gateway.</p>"
    -
        question: "Non riesco a connettermi a Diaspora* con il mio account Disroot."
        answer: "<p>Visto che <b>Diaspora*</b> non supporta <b>LDAP</b> (il protocollo che utilizziamo per accedere ai vari servizi con un unico account) non è possibile loggarsi utilizzando le credenziali di <b>Disroot</b>. Necessiti quindi di creare delle altre credenziali <a href='https://pod.disroot.org/' target='_blank'>qui</a>. However, registrations are currently closed.</p>"

---
