---
title: 'FAQ'
bgcolor: '#FFF'
fontcolor: '#327E82'
text_align: left
part: "VERSCHLUSSELUNG"
section_number: "200"
faq:
    -
        question: "Kann ich 2FA benutzen ?"
        answer: "<p>Ja, Du kannst die 2-Faktor-Autorisierung (2FA) für die <b>Cloud</b> nutzen. Aber, bevor Du sie aktivierst, stell sicher, dass Du vollständig verstanden hast, wie sie funktioniert und wie sie zu benutzen ist. Weitere Informationen findest Du <a href='https://howto.disroot.org/de/tutorials/cloud/introduction#two-factor-authentication' target='_blank'>hier</a></p>"
    -
        question: "Kann ich Ende-zu-Ende-Verschlüsselung in der Cloud nutzen?"
        answer: "<p>Aktuell ist die Ende-zu-Ende-Verschlüsselung aufgrund eines Langzeit-Bugs in der <b>Nextcloud</b>-Desktop-App deaktiviert.</p>"

---
