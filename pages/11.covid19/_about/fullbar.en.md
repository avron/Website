---
title: Covid-19 Kit
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
text_align: center
body_classes: modular
---

<br>
## Coronavirus outbreak !
The Coronavirus outbreak has put us all in a unique situation. A large majority of the world's population must remain in their homes as a measure to prevent the disease from spreading further. This has generated a collapse in a number of services that depend on the Internet and that is why, here in Disroot, we decided to collaborate by making available to all some useful tools so that everyone can communicate, work and share: the Disroot Covid-19 Kit.

## These services do not require registration.

We also want to help other organizations and small companies by providing customized private instances for a fee that is affordable.
