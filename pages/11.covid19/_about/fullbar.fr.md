---
title: Kit Covid19
bgcolor: '#1F5C60'
fontcolor: '#FFFFFF'
text_align: center
body_classes: modular
---

<br>
## Epidémie de coronavirus !
L'épidémie de coronavirus nous a tous mis dans une situation unique. Une grande majorité de la population mondiale doit rester chez elle pour éviter que la maladie ne se propage davantage. Cela a entraîné l'effondrement d'un certain nombre de services qui dépendent d'Internet et c'est pourquoi, ici au sein de Disroot, nous avons décidé de collaborer en mettant à la disposition de tous quelques outils utiles pour que chacun puisse communiquer, travailler et partager : le kit Disroot Covid-19.

## Ces services ne nécessitent pas d'inscription.

Nous voulons également aider d'autres organisations et petites entreprises en fournissant des instances privées personnalisées pour un prix abordable.
