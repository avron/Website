---
title: 'Domain Linking Form'
header_image: netneutrality.jpg
form:
    name: Domain-linking-form
    fields:
        -
            name: username
            label: 'Benutzername'
            placeholder: 'Gib Deinen Disroot-Benutzernamen ein'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: Domain
            label: 'Deine Domain'
            placeholder: Deine Domain
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: 'Alias 1'
            label: Alias
            placeholder: Dein Alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: 'Alias 2'
            label: 'Zweiter Alias (optional)'
            placeholder: Dein Alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 3'
            label: 'Dritter Alias (optional)'
            placeholder: Dein Alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 4'
            label: 'Vierter Alias (optional)'
            placeholder: Dein Alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 5'
            label: 'Fünfter Alias (optional)'
            placeholder: Dein Alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: contribution
            label: 'Spenden per'
            placeholder: select
            type: select
            options:
                patreon: Patreon
                paypal: Paypal
                bank: Banküberweisung
                faircoin: Faircoin
                bitcoin: Bitcoin
            validate:
                required: true
        -
            name: amount
            label: Betrag
            placeholder: 'EUR/USD/BTC/etc.'
            type: text
            validate:
                pattern: '[A-Za-z0-9., ]*'
                required: true
        -
            name: frequency
            label: Zahlweise
            type: radio
            default: monthly
            options:
                monthly: Monatlich
                yearly: Jährlich
            validate:
                required: true
        -
            name: reference
            label: Verwendungszweck
            type: text
            placeholder: 'Name der überweisenden Person oder andere Referenz'
            validate:
                required: true
        -
            name: comments
            label: Anmerkung
            type: textarea
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Absenden
        -
            type: reset
            value: Zurücksetzen
    process:
        -
            email:
                from: domain-request@disroot.org
                to: '{{ form.value.username }}@disroot.org'
                subject: '[Disroot] Deine Anfrage auf Domain-Link'
                body: '<br><br>Hi {{ form.value.username|e }}, <br><br><strong>Vielen Dank für Deine Spende an Disroot.org!</strong><br>Wir freuen uns sehr über Deine Großzügigkeit.<br><br>Wir werden Deine Anfrage bearbeiten und uns so schnell wie möglich wieder bei Dir melden.</strong><br><br><hr><br><strong>Hier nochmal eine Zusammenfassung Deiner Anfrage:</strong><br><br>{% include ''forms/data.html.twig'' %}'
        -
            email:
                from: alias-request@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.username }}@disroot.org'
                subject: '[Domain linking request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: 'Deine Anfrage auf Domain-Verknüpfung wurde versendet!'
        -
            display: thankyou
---

<h1 class="form-title"> Antrag auf Domain-Verknüpfung </h1>
<p class="form-text"><strong> Füll dieses Formular aus, wenn Du Deine eigene Domain für Email-Aliase nutzen möchtest.</strong>
<br><br>
Bitte folge diesen Schritten, um Deine Domain auf unseren Server zeigen zu lassen, bevor Du Deine Anfrage einreichst <br><strong> (dies dient zusätzlich zur Verifizierung, dass die Domain wirklich Dir gehört, und ohne das werden wir Deine Anfrage nicht weiter bearbeiten):</strong><br></p>
<h4 class="form-text"> 1. Gehe in die DNS-Record-Einstellungen für Deine Domain</h4>
<p class="form-text"><b>-</b> <i>Richte den <b>MX Eintrag</b> auf disroot.org (wenn Du derzeit Deine Domain mit einem anderen Provider nutzt, setze <b>MX20</b> oder höher</i><br>
<b>-</b> <i>Setze den TXT (spf) Eintrag auf </i><b>"v=spf1 mx a ptr include:disroot.org ~all"</b><br>
<b>-</b> <i>Setze den TXT Eintrag auf </i><b>"v=spf1 mx ~all"</b><br>
</p>
<h4 class="form-text">2. Beginne zu spenden</h4>
<p class="form-text">Das Email-Alias-Feature wird als <b>"Belohnung"</b> für regelmäßige Unterstützer angeboten. Bei regelmäßigen Unterstützern denken wir an diejenigen, die einen Beitrag von mindestens dem Äquivalent einer Tasse Kaffee im Monat leisten. Damit die Belohnung nicht zur einzigen Motivation für eine Spende wird, erwarten wir, dass die Spende ausgeführt wird, bevor wir dieses Feature aktivieren.<br><br>
Schau Dir die <a href="https://disroot.org/donate">Spendenseite</a> an, um mehr zu erfahren.
</p>
<h4 class="form-text">3. Wenn wir Deine Anfrage bearbeitet haben, werden wir Dich kontaktieren, um die Prozedur zu beenden.</h4>
 <p class="form-text">
Nimm Dir ruhig Zeit, die Höhe Deines Beitrags abzuwägen. Wenn Du uns einen Rio de Janeiro-Kaffee im Monat "kaufen" kannst, ist das OK. Wenn Du Dir jedoch einen Doppelten entkoffeinierten Soja Frappuccino mit einem extra Shot und Sahne im Monat leisten kannst, dann hilfst Du uns wirklich, die Disroot-Plattform am Laufen zu halten und sicherzugehen, dass sie frei zugänglich bleibt für andere Menschen mit weniger Mitteln.
 </p>
