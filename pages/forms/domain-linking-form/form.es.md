---
title: 'Formulario de vinculación de Dominio'
header_image: netneutrality.jpg
form:
    name: Domain-linking-form
    fields:
        -
            name: username
            label: 'Nombre de usuario'
            placeholder: 'Ingresa tu nombre de usuario de Disroot'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: Domain
            label: 'Tu dominio'
            placeholder: tu dominio
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: 'Alias 1'
            label: Alias
            placeholder: tu alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: 'Alias 2'
            label: 'Segundo Alias (opcional)'
            placeholder: tu alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 3'
            label: 'Tercer Alias (opcional)'
            placeholder: tu alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 4'
            label: 'Cuarto Alias (opcional)'
            placeholder: tu alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 5'
            label: 'Quinto Alias (opcional)'
            placeholder: tu alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: contribution
            label: 'Contribuir a través de'
            placeholder: select
            type: select
            options:
                patreon: Patreon
                paypal: Paypal
                bank: 'Transferencia bancaria'
                faircoin: Faircoin
                bitcoin: Bitcoin
            validate:
                required: true
        -
            name: amount
            label: Cantidad
            placeholder: 'EUR/USD/BTC/etc.'
            type: text
            validate:
                pattern: '[A-Za-z0-9., ]*'
                required: true
        -
            name: frequency
            label: Frecuencia
            type: radio
            default: monthly
            options:
                monthly: Mensual
                yearly: Anual
            validate:
                required: true
        -
            name: reference
            label: Referencia
            type: text
            placeholder: 'Nombre del titular del pago u otras referencias de la transferencia'
            validate:
                required: true
        -
            name: comments
            type: textarea
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Enviar
        -
            type: reset
            value: Reiniciar
    process:
        -
            email:
                from: domain-request@disroot.org
                to: '{{ form.value.username }}@disroot.org'
                subject: '[Disroot] Tu solicitud de vinculación de Dominio'
                body: '<br><br>Hola {{ form.value.username|e }}, <br><br><strong>¡Gracias por tu contribución a Disroot.org!</strong><br>Apreciamos sinceramente tu generosidad.<br><br>Revisaremos tu solicitud y te responderemos tan pronto como podamos.</strong><br><br><hr><br><strong>Aquí está un resumen de la solicitud recibida:</strong><br><br>{% include ''forms/data.html.twig'' %}'
        -
            email:
                from: alias-request@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.username }}@disroot.org'
                subject: '[Domain linking request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: '¡Tu solicitud de vinculación de Dominio ha sido enviada!'
        -
            display: Gracias
---

<h1 class="form-title"> Solicitud de vinculación de Dominio </h1>
<p class="form-text"><strong> Completa este formulario si quieres utilizar tu propio dominio para los alias de correo.</strong>
 <br><br>
Por favor, Sigue estos pasos para apuntar tu dominio a nuestro servidor: <br><strong> (esto también servirá para verificar que el dominio realmente te pertenece) </strong><br>
    1. Ve a la configuración de registros DNS de tu dominio<br>
    2. Apunta el registro MX 10 a disroot.org<br>
    3. Apunta el registro SPF a "v=spf1 mx a ptr include:disroot.org ~all"<br>
    4. Apunta el registro TXT a "v=spf1 mx ~all"<br>
    5. Una vez que que revisemos tu solicitud, te contactaremos para finalizar el procedimiento<br>
 <br>
<strong>Por favor, tómate el tiempo para considerar tu contribución.</strong> Si puedes 'comprarnos' una taza de café al mes en Río De Janeiro eso está bien, pero si puedes permitirte pagar un Café Doble Descafeinado Con Crema extra al mes, entonces puedes ayurdarnos en serio a mantener la plataforma Disroot corriendo y asegurar que esté disponible de manera gratuita para otras personas con menos recursos.
 </p>
