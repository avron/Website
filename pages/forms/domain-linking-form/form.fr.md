---
title: 'Formulaire pour lier un domaine'
header_image: netneutrality.jpg
form:
    name: Domain-linking-form
    fields:
        -
            name: username
            label: "Nom d'utilisateur"
            placeholder: "Entrez votre nom d'utilisateur Disroot"
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: Domain
            label: 'Votre domaine'
            placeholder: votre-domaine
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: 'Alias 1'
            label: Alias
            placeholder: votre-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: 'Alias 2'
            label: 'Deuxième Alias (optionnel)'
            placeholder: votre-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 3'
            label: 'Troisième Alias (optionnel)'
            placeholder: votre-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 4'
            label: 'Quatrième Alias (optionnel)'
            placeholder: votre-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 5'
            label: 'Cinquième Alias (optionnel)'
            placeholder: votre-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: contribution
            label: 'Contribuant via'
            placeholder: sélectionner
            type: select
            options:
                patreon: Patreon
                paypal: Paypal
                bank: 'Virement bancaire'
                faircoin: Faircoin
                bitcoin: Bitcoin
            validate:
                required: true
        -
            name: amount
            label: Montant
            placeholder: 'EUR/USD/BTC/etc.'
            type: text
            validate:
                pattern: '[A-Za-z0-9., ]*'
                required: true
        -
            name: frequency
            label: Répété
            type: radio
            default: monthly
            options:
                monthly: Mensuellement
                yearly: Annuellement
            validate:
                required: true
        -
            name: reference
            label: Reference
            type: text
            placeholder: 'Nom du titulaire du paiement ou autre référence de transfert'
            validate:
                required: true
        -
            name: comments
            type: textarea
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Envoyer
        -
            type: reset
            value: Réinitialiser
    process:
        -
            email:
                from: domain-request@disroot.org
                to: '{{ form.value.username }}@disroot.org'
                subject: '[Disroot] Votre demande de lien vers un domaine'
                body: "<br><br>Bonjour {{ form.value.username|e }}, <br><br><strong>Merci de votre contribution à Disroot.org!</strong><br>Nous apprécions vraiment votre générosité.<br><br>Nous examinerons votre demande d'alias et vous répondrons dès que possible..</strong><br><br><hr><br><strong>Voici un résumé de la demande reçue:</strong><br><br>{% include ''forms/data.html.twig'' %}"
        -
            email:
                from: alias-request@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.username }}@disroot.org'
                subject: '[Domain linking request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: "Votre demande de lien vers un domaine a été envoyée!"
        -
            display: Merci
---

<h1 class="form-title"> Formulaire pour lier un domaine </h1>
<p class="form-text"><strong> Remplissez ce formulaire si vous souhaitez utiliser votre propre domaine pour les alias Email.</strong>
<br><br>
Veuillez suivre ces étapes pour diriger votre domaine vers notre serveur avant de soumettre votre demande : <br><strong> (ceci servira aussi à vérifier que le domaine vous appartient vraiment et sans cela nous ne traiterons pas votre demande) </strong><br></p>
<h4 class="form-text"> 1. Allez dans les paramètres des enregistrements DNS pour votre domaine</h4>.
<p class="form-text"><b>-</b> <i>Diriger votre <b>enregistrement MX</b> vers disroot.org (si vous utilisez votre domaine actuellement avec un autre fournisseur, réglez sur <b>MX20</b> ou plus)</i><br>
<b>-</b> <i>Réglez l'enregistrement TXT (spf) ainsi </i><b>"v=spf1 mx a ptr include:disroot.org ~all"</b><br>
<b>-</b> <i>Régler l'enregistrement TXT ainsi </i><b>"v=spf1 mx ~all"</b><br>
</p>
<h4 class="form-text">2. Commencez à faire un don </h4>
<p class="form-text">La fonction d'alias de courriel est offerte sous forme de <b>"récompense"</b> pour les donateurs réguliers. Par donateurs réguliers, nous pensons à ceux qui contribuent par, au moins, l'équivalent d'une tasse de café par mois. Afin que la " récompense " ne devienne pas la seule incitation à faire un don, nous nous attendons à ce que le don soit fait avant que nous puissions activer cette fonction. <br><br>
Lisez la <a href="https://disroot.org/donate">page de donation</a> pour en savoir plus.
</p>
<h4 class="form-text">3. Une fois votre demande examinée, nous vous contacterons  pour finaliser la procédure. </h4>
<p class="form-text">
Veuillez prendre le temps de réfléchir à votre contribution. Si vous pouvez nous 'acheter' une tasse de café Rio De Janeiro par mois, c'est bien, mais si vous pouvez vous permettre un Frappuccino de soja double décaféiné avec une dose supplémentaire et de la crème tous les mois, vous pouvez réellement nous aider à maintenir la plateforme Disroot et assurer que celle-ci soit accessible gratuitement pour les personnes moins avantagées.
</p>
