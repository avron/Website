---
title: 'Modulo di collegamento del dominio'
header_image: netneutrality.jpg
form:
    name: Domain-linking-form
    fields:
        -
            name: username
            label: 'Nome Utente'
            placeholder: 'Inserisci il tuo nome utente disroot'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: Domain
            label: 'Il tuo dominio'
            placeholder: il-tuo-dominio
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: 'Alias 1'
            label: Alias
            placeholder: il-tuo-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: 'Alias 2'
            label: 'Secondo Alias (facoltativo)'
            placeholder: il-tuo-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 3'
            label: 'Terzo Alias (facoltativo)'
            placeholder: il-tuo-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 4'
            label: 'Quarto Alias (facoltativo)'
            placeholder: il-tuo-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 5'
            label: 'Quinto Alias (facoltativo)'
            placeholder: il-tuo-alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: contribution
            label: 'Contributo tramite'
            placeholder: select
            type: select
            options:
                patreon: Patreon
                paypal: Paypal
                bank: 'Bonifico bancario'
                faircoin: Faircoin
                bitcoin: Bitcoin
            validate:
                required: true
        -
            name: amount
            label: Importo
            placeholder: 'in €, $, BTC etc.'
            type: text
            validate:
                pattern: '[0-9.,]*'
                required: true
        -
            name: frequency
            label: Frequenza
            type: radio
            default: monthly
            options:
                monthly: Mensile
                yearly: Annuale
            validate:
                required: true
        -
            name: reference
            label: Riferimento
            type: text
            placeholder: 'Nome del titolare del pagamento o altro riferimento di trasferimento'
            validate:
                required: true
        -
            name: comments
            type: textarea
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Invia
        -
            type: reset
            value: Azzera
    process:
        -
            email:
                from: domain-request@disroot.org
                to: '{{ form.value.username }}@disroot.org'
                subject: '[Disroot] La tua richiesta di collegamento al dominio'
                body: '<br><br>Hi {{ form.value.username|e }}, <br><br><strong>Grazie per il tuo contributo a Disroot.org!</strong><br>Apprezziamo davvero la sua generosità.<br><br>Esamineremo la vostra richiesta e vi risponderemo appena possibile.</strong><br><br><hr><br><strong>Ecco una sintesi della richiesta ricevuta:</strong><br><br>{% include ''forms/data.html.twig'' %}'
        -
            email:
                from: alias-request@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.username }}@disroot.org'
                subject: '[Domain linking request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: 'La sua richiesta di collegamento al dominio è stata inviata!'
        -
            display: grazie
---

<h1 class="form-title"> Richiesta di collegamento di dominio </h1>
<p class="form-text"><strong> Compila questo modulo se desideri utilizzare il tuo dominio per gli alias e-mail.</strong>
 <br><br>
Segui questi passi per puntare il tuo dominio al nostro server prima di procedere con la richiesta: 

<br><strong> (Questo servirà per verificare che il dominio sia veramente di tua proprietà. Senza questo step non procederemo alla tua richiesta) </strong><br></p>
<h4 class="form-text"> 1. Vai nelle impostazioni DNS records del tuo dominio</h4>
<p class="form-text"><b>-</b> <i>Punta <b>MX record</b> su disroot.org (se stai utilizzando il tuo dominio con un altro provider, imposta <b> MX20 </b> o superiore))</i><br>
<b>-</b> <i>Imposta TXT (spf) record su </i><b>"v=spf1 mx a ptr include:disroot.org ~all"</b><br>
<b>-</b> <i>Imposta TXT record su </i><b>"v=spf1 mx ~all"</b><br>
</p>
<h4 class="form-text">2. Fai una donazione</h4>
<p class="form-text">L'alias email è offerto come <b>"ricompensa"</b> per i sostenitori regolari. Come sostenitori regolari intendiamo coloro che contribuiscono con almeno l'equivalente di una tazza di caffè al mese.<br> <br>
Controlla <a href="https://disroot.org/donate"> pagina di donazione </a> per saperne di più.

</p>
<h4 class="form-text">3. Una volta esaminata la tua richiesta ti contatteremo per finalizzare la procedura.</h4>
<p class="form-text">


<br>
<strong>Per favore, prenditi il tempo per considerare il tuo contributo.</strong> Se puoi 'comprarci' una tazza di caffè Rio De Janeiro al mese va bene, ma se puoi permetterti un Frappuccino di soia a doppio decaffeinato con extra e crema al mese, allora puoi davvero aiutarci a mantenere in funzione la piattaforma Disroot e assicurarti che sia disponibile gratuitamente per altre persone con meno mezzi.
 </p>
