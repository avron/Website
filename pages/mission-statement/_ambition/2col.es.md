---
title: Declaración de Objetivos
bgcolor: '#1F5C60'
fontcolor: '#FFF'
---

---

### Las ambiciones

La Fundación Disroot.org tiene ambiciones que exceden la mera operación de la plataforma. La intención no es volverse un proveedor centralizado sino promover y sostener un ecosistema que ayude a otros a establecer sus propios nodos independientes. Uno de los más importantes objetivos a largo plazo es proporcionar espacio para otras organizaciones, individuos y colectivos que quieran comenzar su propio camino, para que no tengan que reinventar la rueda una y otra vez. Queremos incentivar el surgimiento de más proveedores independientes de servicios descentralizados que trabajen cooperativamente, y no en competencia, unos con otros. Juntos podemos construir una red descentralizada verdaderamente fuerte.

Abiertamente promovemos e incentivamos el nacimiento de nuevos nodos federados y esperamos poder ayudar a otros grupos a desarrollar sus plataformas similares a Disroot. Para lograrlo, trabajamos en un conjunto de herramientas que faciliten la implementación, el mantenimiento y la administración de las mismas. Otras de nuestras ambiciones son crear un espacio para compartir habilidades e intercambiar conocimientos; una plataforma de asistencia legal para ayudar a otros a lidiar con las reglas, las leyes, las regulaciones y sus aplicaciones; ayudar en la creación de nuevos nodos y contribuir patrocinando el desarrollo de Software Libre y de Código Abierto.
