---
title: Declaración de Objetivos
bgcolor: '#FFF'
fontcolor: '#1F5C60'
---

---

### Los aspectos prácticos

Para que el proyecto Disroot.org funcione consecuentemente con los principios antes mencionados, operamos de acuerdo a estos otros principios prácticos:

##### 1) Financiamiento

Disroot.org es una organización sin fines de lucro y todos los ingresos de la plataforma Disroot son invertidos de nuevo en el proyecto, el mantenimiento de servicios y en desarrollo de software libre.

Reconocemos diferentes maneras de financiar al proyecto y su desarrollo.

A. Donaciones de usuarios y usuarias.
<br>
Pensamos que poner un precio a nuestros servicios es, de alguna manera, limitar la autonomía de los usuarios y las usuarias. En consecuencia, confiamos esta responsabilidad a los y las Disrooters. Entendemos que el valor del dinero varía de acuerdo a diferentes factores, como la ubicación geográfica, la condición económica o la situación financiera personal, así que decidimos que sean las y los Disrooters mismos quienes evalúen con cuánto pueden contribuir al proyecto.
<br>
Es importante resaltar el hecho de que nada en la Internet es gratis y que los servicios en línea siempre tienen un costo que se paga con dinero de verdad o con importante información privada. Estamos seguros que la libertad de internet solo se puede lograr y sostener cuando las personas toman conciencia de esto y contribuyen voluntariamente. Como Disroot.org no vende información de sus usuarios y usuarias, dependemos fuertemente de las donaciones.

B. Subsidios o subvenciones.
<br>
La recepción de subvenciones y otras formas de financiación solo serán aceptadas si no perjudican en modo alguno la independencia del proyecto. No pondremos en peligro la privacidad de lxs usuarixs o nuestra libertad y la forma en que queremos gestionar el servicio, para cumplir con los requisitos establecidos por los "donantes". Somos conscientes que las subvenciones suelen asignarse con un propósito o resultado pero también estamos convenidos que algunos de esos requerimientos pueden seguirse sin perjudicar al proyecto o a la red federada independiente en su conjunto. Consideraremos cuidadosamente cada posible subsidio antes de aceptarlo para asegurarnos que no contradice los principios más importantes del proyecto

##### 2) Proceso de toma de decisiones
Seguimos un proceso de decisión basado en el consenso. Dentro del Equipo Central de Disroot todos tienen igual voz y opinión. Todas las decisiones importantes se toman durante las reuniones en las que se exponen propuestas, que se debaten ampliamente para que todos tengan tiempo de elaborar sus opiniones o preocupaciones antes de llegar a un acuerdo mutuo.

##### 3) Inclusión de la comunidad
Disroot.org no tendría sentido sin la vibrante comunidad que lo rodea, cuya participación activa en el proyecto es muy importante y fomentada. Agradecemos cualquier ayuda, mejoras, comentarios y sugerencias. Ofrecemos varias formas de comunicación entre los miembros de la comunidad de Disroot y el Equipo Central, tales como: foros, varios tableros de proyectos de Taiga o las salas de mensajería instantánea.
